<page>
  <title>Упражнениe</title>
  <content>
    <div class="flex one two-800 three-1000 four-1200">{
      for $problemo in ekzamenoj/ekzameno/problemoj/problemo[similaj]
      let $dato := data($problemo/../../dato[1]), $numero := data($problemo/numero), $url := data($problemo/url), $bildeto := data($problemo/bildeto), $similaj := data($problemo/similaj)
      return
      <div>
        <article class="card">
          <header>{$numero} задача от {$dato}</header>
          <a href="{$url}">
            <img class="responsive-image" src="{$bildeto}" alt="Задача {$numero} от {$dato}"/>
          </a>
          <footer>
            <a href="{replace(replace($similaj, '^.*/', ''), 'xml', 'xhtml')}">
              <img src="bootstrap-icons-1.5.0/pencil.svg" alt="Pencil" class="icon"/>
              Подобни задачи за упражнение
            </a>
          </footer>
        </article>
      </div>
    }</div>
  </content>
</page>
